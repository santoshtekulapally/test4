//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {


    // MARK: Outlets for sprites
    var Point:Int!
    var player:SKSpriteNode!
    let PLAYER_SPEED:CGFloat = 40
    var Stop:SKSpriteNode!
    var BiggWall:SKSpriteNode!
     var Flag:SKSpriteNode!
     var Is:SKSpriteNode!
     var win:SKSpriteNode!
    //var flagwin:SKSpriteNode!
 var superwin:SKSpriteNode!
   
    
    func showAlert(withTitle title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { _ in }
        alertController.addAction(okAction)
        
        view?.window?.rootViewController?.present(alertController, animated: true)
    }

   


    override func didMove(to view: SKView) {
        Point = 0
        self.physicsWorld.contactDelegate = self

        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.Stop = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.BiggWall = self.childNode(withName: "wall") as! SKSpriteNode
        self.Flag = self.childNode(withName: "flagblock") as! SKSpriteNode
        self.Is = self.childNode(withName: "isblock") as! SKSpriteNode
        self.win = self.childNode(withName: "winblock") as! SKSpriteNode
       // self.flagwin = self.childNode(withName: "flag") as! SKSpriteNode
        self.superwin = self.childNode(withName: "loo") as! SKSpriteNode

    }
   
    func didBegin(_ contact: SKPhysicsContact) {
       // print("Something collided!")
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
       
       
        
        if (nodeA == nil || nodeB == nil) {
            return
        }
        
        if (nodeA!.name == "player" && nodeB!.name == "loo") {
            
            player.physicsBody?.isDynamic = false

           if(Point > 1)
           {
            print("player win yay")
            
           showAlert(withTitle: "Winner Winner ", message: "You Won")
            
            }
           else{
            print("no win yet")
            }
            
        }
        if (nodeA!.name == "player" && nodeB!.name == "stopblock") {
           
            
            player.physicsBody?.isDynamic = false
           
        }
        if (nodeA!.name == "flagblock" && nodeB!.name == "isblock"){
            
            print("Wait for the winbox")
            
            Point = Point + 1;
            // player die
            print("win value is  \(Point!).")

        }
        if (nodeA!.name == "player" && nodeB!.name == "flagblock") {
            
            player.physicsBody?.isDynamic = true

            
        }
        if (nodeA!.name == "winblock" && nodeB!.name == "isblock") {
            
            

            Point = Point + 1;
            // player die
             //print("Wait for the flagbox")
            print("win value is  \(Point!).")

        }
                 if (nodeA!.name == "player" && nodeB!.name == "winblock") {
        
        
        
        Point = Point + 1;
        // player die
                    print("win value is \(Point!).")

    }
    //
//        if (nodeA!.name == "player" && nodeB!.name == "flag") {
//            
//            print("player won")
//            // player die
//            
//        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
        
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upButton") {
            // move up
            self.player.position.y = self.player.position.y + PLAYER_SPEED
        }
        else if (nodeTouched == "downButton") {
            // move down
            self.player.position.y = self.player.position.y - PLAYER_SPEED
        }
        else if (nodeTouched == "leftButton") {
            // move left
            self.player.position.x = self.player.position.x - PLAYER_SPEED
        }
        else if (nodeTouched == "rightButton") {
            // move right
            self.player.position.x = self.player.position.x + PLAYER_SPEED
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
   
}
